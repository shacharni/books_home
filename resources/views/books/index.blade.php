@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<h1> this is a books list</h1>


    <head>
    </head>
    <body>
    
    <table>
            <tr>
            
           
            <th></th>
                <th color="red">ID</th>
                <th>title</th>
                <th>author</th>
            </tr>
            @foreach($books as $book)
            <tr>
            <td>@if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" value="1"  checked>
           read!
       @else
           <input type = 'checkbox' id ="{{$book->id}}" value="0">
           not read
       @endif

</td>
                <td color="red">{{$book->id}}</td>
                <td>{{$book->title}}</td>
                <td>{{$book->author}}</td>
                <td><a href="{{route('books.edit',$book->id)}}">edit</a></td>
                @can('admin')  <td>
                 <form method = 'post' action="{{action('BookController@destroy', $book->id)}}">   @csrf   @method('DELETE')
                 <div class = "form-group">

               <input type ="submit" class = "form-control" name="submit" value ="Delete task">

            </div>



        </form></td>@endcan
            </tr>
            @endforeach
        </table>
       

        <a href="{{route('books.create')}}">Create a new book</a>
        <script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
              console.log(event.target.id)
               $.ajax({
                   url: "{{url('books')}}"+'/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:"{{csrf_token()}}"}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
       
   </script>  


   </body>
</html>
@endsection