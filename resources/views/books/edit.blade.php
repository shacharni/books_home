@extends('layouts.app')

@section('content')
<h1>Update a book</h1>
<form method='post' action="{{action('BookController@update',$book->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
    <label for ="edit"> book to update</label>
        <label for ="title"> new title</label>
        <input type="text" class= "form-control" name="title" value="{{$book->title}}">
        <label for ="author"> new author</label>
        <input type="text" class= "form-control" name="author" value="{{$book->author}}">
    </div>
    
    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="update">
    </div>
</form>
@endsection