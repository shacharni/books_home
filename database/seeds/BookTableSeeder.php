<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
             'title' => 'book 1',
             'author'=>'author 1',
            'user_id'=>'1',
        ],
        [
            'title' => 'book 2',
            'author'=>'author 2',
           'user_id'=>'1',
        ],
        [
            'title' => 'book 3',
            'author'=>'author 3',
           'user_id'=>'1',
        ],
        [
            'title' => 'book 4',
            'author'=>'author 4',
           'user_id'=>'1',
        ],
        
        ]);
       
    }
}
